package advance.android.formmvprealm;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import advance.android.formmvprealm.RealmMVP.Contract;
import advance.android.formmvprealm.RealmMVP.Presenter;
import advance.android.formmvprealm.RealmMVP.UserEntity;
import advance.android.formmvprealm.RealmMVP.UsersAdapter;
import advance.android.formmvprealm.custom_views.MyEditText;
import advance.android.formmvprealm.utils.BaseActivity;

public class ActivityMain extends BaseActivity implements Contract.View, UsersAdapter.SelectButton {

    Contract.Presenter presenter = new Presenter();
    int userId = 0;
    MyEditText txtName, txtLastName, txtEmail, txtAge;
    RecyclerView recycler;
    UserEntity userEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();

        presenter.attachView(this);
    }
//
    @Override
    public void onListLoaded(List<UserEntity> userList) {

        UsersAdapter adapter = new UsersAdapter(mContext, userList);
        recycler.setAdapter(adapter);

        adapter.setSelectButton(this);

    }

    void bind() {
        txtName = findViewById(R.id.txtName);
        txtLastName = findViewById(R.id.txtLastName);
        txtEmail = findViewById(R.id.txtEmail);
        txtAge = findViewById(R.id.txtAge);


        recycler = findViewById(R.id.recycler);

        findViewById(R.id.btnSave).setOnClickListener(v -> {

            userEntity = new UserEntity(userId, txtName.text(), txtLastName.text(), txtEmail.text(), !txtAge.text().equals("") ? Integer.parseInt(txtAge.text()) : 0);

            presenter.saveData(userEntity);

            clearEditViews();
        });

    }

    void clearEditViews() {
        txtName.setText("");
        txtLastName.setText("");
        txtEmail.setText("");
        txtAge.setText("");
        userId = 0;
    }

    @Override
    public void onDeleteSelected(UserEntity userEntity) {
        presenter.deleteUser(userEntity);
    }

    @Override
    public void onEditSelected(UserEntity userEntity) {
        txtName.setText(userEntity.getName());
        txtLastName.setText(userEntity.getLastname());
        txtEmail.setText(userEntity.getEmail());
        txtAge.setText(userEntity.getAge() + "");

        userId = userEntity.getId();
    }
}
