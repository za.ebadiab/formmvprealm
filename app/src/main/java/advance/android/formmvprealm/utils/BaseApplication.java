package advance.android.formmvprealm.utils;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import io.realm.Realm;

public class BaseApplication extends Application {

    public static Typeface typeface;
//    static BaseApplication baseApp;

    @Override
    public void onCreate() {
        super.onCreate();

//        baseApp = this;

        typeface = Typeface.createFromAsset(getAssets(), Constants.font_name);

        Realm.init(this);
    }
}
