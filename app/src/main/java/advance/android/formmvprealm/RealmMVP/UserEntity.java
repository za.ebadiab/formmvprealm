package advance.android.formmvprealm.RealmMVP;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

//public class UserEntity extends RealmObject {
public class UserEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private String name;
    private String lastname;
    private String email;

    private int age;

    public UserEntity() {
    }

    public UserEntity(int id, String name, String lastname, String email, int age) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.age = age;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                '}';
    }
}
