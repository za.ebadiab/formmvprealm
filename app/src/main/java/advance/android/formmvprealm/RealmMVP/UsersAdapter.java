package advance.android.formmvprealm.RealmMVP;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import java.util.List;

import advance.android.formmvprealm.R;
import advance.android.formmvprealm.custom_views.MyTextView;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.Holder> {

    private Context mContext;
    private List<UserEntity> userEntityList;

    public UsersAdapter(Context mContext, List<UserEntity> userEntityList) {
        this.mContext = mContext;
        this.userEntityList = userEntityList;
    }

    @NonNull
    @Override
    public UsersAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.users_recyclers_item, parent, false);

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersAdapter.Holder holder, int position) {
        holder.txtNameLastname.setText(userEntityList.get(position).getName() + " " + userEntityList.get(position).getLastname());
        holder.txtEmail.setText(userEntityList.get(position).getEmail());
    //   String age = userEntityList.get(position).getAge()+" :"+mContext.getResources().getString(R.string.enter_age) ;


    //    holder.txtAge.setText(userEntityList.get(position).getAge()+" :"+ R.string.enter_age);
        holder.txtAge.setText(mContext.getResources().getString(R.string.enter_age)+" : "+userEntityList.get(position).getAge());
    }

    @Override
    public int getItemCount() {
        return userEntityList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        MyTextView txtNameLastname, txtEmail , txtAge;

        Holder(View itemView) {
            super(itemView);

            txtNameLastname = itemView.findViewById(R.id.txtNameLastname);
            txtEmail = itemView.findViewById(R.id.txtEmail);
            txtAge = itemView.findViewById(R.id.txtAge);

            itemView.findViewById(R.id.imgDelete).setOnClickListener(v -> {
                UserEntity userEntity = userEntityList.get(getAdapterPosition());

                openDeleteDialog(userEntity);

            });

            itemView.findViewById(R.id.imgEdit).setOnClickListener(v -> {
                UserEntity userEntity = userEntityList.get(getAdapterPosition());
                selectButton.onEditSelected(userEntity);
            });

        }


        void openDeleteDialog(UserEntity userEntity) {


            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.delete_yesno_dialog);


            dialog.findViewById(R.id.btnYes).setOnClickListener((View v) -> {
                selectButton.onDeleteSelected(userEntity);
                dialog.dismiss();
            });
            dialog.findViewById(R.id.btnNo).setOnClickListener((View v) -> {

                dialog.dismiss();
            });

            dialog.show();
        }


    }

    private SelectButton selectButton;

    public void setSelectButton(SelectButton selectButton) {
        this.selectButton = selectButton;
    }

    public interface SelectButton {
        void onDeleteSelected(UserEntity userEntity);

        void onEditSelected(UserEntity userEntity);
    }
}
