package advance.android.formmvprealm.RealmMVP;

import android.util.Log;

import io.realm.Realm;
import io.realm.RealmResults;

public class Model implements Contract.Model {
    private Contract.Presenter presenter;
    private Realm realm = Realm.getDefaultInstance();


    @Override
    public void attachPresenter(Contract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void saveData(UserEntity userEntity) {

        realm.beginTransaction();

        UserEntity result;
        if (userEntity.getId() != 0) {
            result = realm.where(UserEntity.class).equalTo("id", userEntity.getId()).findFirst();
            result.setName(userEntity.getName());
            result.setLastname(userEntity.getLastname());
            result.setEmail(userEntity.getEmail());
            result.setAge(userEntity.getAge());
        } else {
            userEntity.setId(getNextKey());
            realm.copyToRealm(userEntity);
        }

        realm.commitTransaction();

        loadList();

    }

    private int getNextKey() {
        try {
            Number number = realm.where(UserEntity.class).max("id");
            if (number != null) {
                return number.intValue() + 1;
            } else {
                return 1;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }


    @Override
    public void deleteUser(UserEntity userEntity) {
        realm.beginTransaction();

        RealmResults<UserEntity> result = realm.where(UserEntity.class).equalTo("name", userEntity.getName()).equalTo("lastname", userEntity.getLastname()).equalTo("email", userEntity.getEmail()).equalTo("age", userEntity.getAge()).findAll();
        result.deleteFirstFromRealm();

        realm.commitTransaction();

        loadList();
    }

    @Override
    public void loadList() {
        realm.beginTransaction();
        RealmResults<UserEntity> userList = realm.where(UserEntity.class).findAll();
        realm.commitTransaction();

        presenter.onListLoaded(userList);

    }
}
