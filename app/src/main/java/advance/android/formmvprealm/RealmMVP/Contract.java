package advance.android.formmvprealm.RealmMVP;

import java.util.ArrayList;
import java.util.List;

public interface Contract {

    interface View {
        void onListLoaded(List<UserEntity> userList);
    }

    interface Presenter {

        void attachView(View view);

        void saveData(UserEntity userEntity);

        void deleteUser(UserEntity userEntity);

        void onListLoaded(List<UserEntity> userList);

    }

    interface Model {

        void attachPresenter(Presenter presenter);

        void saveData(UserEntity userEntity);

        void deleteUser(UserEntity userEntity);

        void loadList();

    }


}
