package advance.android.formmvprealm.RealmMVP;


import java.util.List;

public class Presenter implements Contract.Presenter {
    private Contract.View view;
    private Contract.Model model = new Model();


    @Override
    public void attachView(Contract.View view) {
        this.view = view;
        model.attachPresenter(this);
        model.loadList();
    }

    @Override
    public void saveData(UserEntity userEntity) {
        model.saveData(userEntity);
    }

    @Override
    public void deleteUser(UserEntity userEntity) {
        model.deleteUser(userEntity);
    }


    @Override
    public void onListLoaded(List<UserEntity> userList) {
        view.onListLoaded(userList);
    }
}
